#!/bin/bash
source ~/anaconda3/etc/profile.d/conda.sh
conda create --name minhpv4 python=3.6
conda activate minhpv4
pip install -r requirements.txt
